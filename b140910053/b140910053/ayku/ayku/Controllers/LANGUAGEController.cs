﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ayku.Controllers
{
    public class LANGUAGEController : Controller
    {
        // GET: LANGUAGE
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Change(string languageabbrevation)
        {
            if (languageabbrevation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageabbrevation);
                Thread.CurrentThread.CurrentUICulture =  new CultureInfo(languageabbrevation);
       
            }
            HttpCookie cookie = new HttpCookie("language");
            cookie.Value = languageabbrevation;
            Response.Cookies.Add(cookie);
            return View("Index");
        
        }
    }
}