﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ayku.Models;

namespace ayku.Controllers
{
    public class urunIstekController : Controller
    {
        private KediEviEntities db = new KediEviEntities();

       

        // GET: urunIstek/Create
        public ActionResult Index()
        {
            ViewBag.id = new SelectList(db.Uyeler, "id", "adsoyad");
            return View();
        }

        // POST: urunIstek/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "istekID,id,mesaj")] urunIstek urunIstek)
        {
            if (ModelState.IsValid)
            {
                db.urunIstek.Add(urunIstek);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Uyeler, "id", "adsoyad", urunIstek.id);
            return View(urunIstek);
        }

     
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
