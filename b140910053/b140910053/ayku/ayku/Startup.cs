﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ayku.Startup))]
namespace ayku
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
