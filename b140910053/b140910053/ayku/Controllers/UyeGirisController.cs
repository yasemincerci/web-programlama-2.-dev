﻿using ayku.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ayku.Controllers
{
    public class UyeGirisController : Controller
    {
        // GET: UyeGiris
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Giris(UyeGirisModel model)
        {
            KediEviEntities db = new KediEviEntities();
            
           
                foreach (var uye in db.Uyeler)
                {
                    if (uye.email == model.email && uye.sifre == model.sifre) // Formdaki Veriler veritabanıyla eşleşiyorsa
                    {
                        if(uye.adsoyad=="admin" && uye.email=="admin@admin.com")
                        {
                            Session["adsoyad"] = uye.adsoyad;
                            Session["admin"] = uye.adsoyad;
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            Session["adsoyad"] = uye.adsoyad;           //Session oluşur ve anasayfaya yönlendirir
                            return RedirectToAction("Index", "Home");
                        }
                        
                    }                  
                }
            
            return RedirectToAction("Index");
        }
        public ActionResult Cikis()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }
    }
}