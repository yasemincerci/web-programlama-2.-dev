﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ayku.Models;

namespace ayku.Controllers
{
    public class iletisimController : Controller
    {
        private KediEviEntities db = new KediEviEntities();

     
  

        // GET: iletisims/Create
        public ActionResult iletisim()
        {
            return View();
        }

        // POST: iletisims/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult iletisim([Bind(Include = "iletisimID,ad,eposta,telefon,iletiniz")] iletisim iletisim)
        {
            if (ModelState.IsValid)
            {
                db.iletisim.Add(iletisim);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(iletisim);
        }

     


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
