﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ayku.Models
{
    public class UyeGirisModel
    {
        [Required(ErrorMessage = ("*Lütfen boş bırakmayınız"))]
        [Display(Name = "Email Adresiniz:")]
        [EmailAddress(ErrorMessage = ("E-mail adresiniz geçerli değil"))]
        public string email { get; set; }

        [Required(ErrorMessage = ("*Lütfen boş bırakmayınız"))]
        [DataType(DataType.Password)]
        [Display(Name = "Şifre:")]
        public string sifre { get; set; }
    }
}