﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ayku.viewmodel
{
    public class urunmodel
    {[Required(ErrorMessage="urunismi girmediniz")]
        [StringLength(50,ErrorMessage="urunismi en fazla 50 karakter")]
        public string urunisim { get; set; }
        public int categoriid { get; set; }
        public HttpPostedFileBase resim { get; set; }
        public List<SelectListItem> categoriliste { get; set; }
    }
}