﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ayku.cokludil {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class dil {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal dil() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ayku.cokludil.dil", typeof(dil).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Barınaklar.
        /// </summary>
        public static string Barınaklar {
            get {
                return ResourceManager.GetString("Barınaklar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dil.
        /// </summary>
        public static string Dİl {
            get {
                return ResourceManager.GetString("Dİl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ev.
        /// </summary>
        public static string ev {
            get {
                return ResourceManager.GetString("ev", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to giriş.
        /// </summary>
        public static string Giriş {
            get {
                return ResourceManager.GetString("Giriş", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda.
        /// </summary>
        public static string Hakkımızda {
            get {
                return ResourceManager.GetString("Hakkımızda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string İletişim {
            get {
                return ResourceManager.GetString("İletişim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt.
        /// </summary>
        public static string Kayıt {
            get {
                return ResourceManager.GetString("Kayıt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to KEDİEVİ.
        /// </summary>
        public static string KEDİEVİ {
            get {
                return ResourceManager.GetString("KEDİEVİ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kedilerhakkında.
        /// </summary>
        public static string Kedilerhakkında {
            get {
                return ResourceManager.GetString("Kedilerhakkında", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to KediShop.
        /// </summary>
        public static string KediShop {
            get {
                return ResourceManager.GetString("KediShop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ol.
        /// </summary>
        public static string Ol {
            get {
                return ResourceManager.GetString("Ol", resourceCulture);
            }
        }
    }
}
